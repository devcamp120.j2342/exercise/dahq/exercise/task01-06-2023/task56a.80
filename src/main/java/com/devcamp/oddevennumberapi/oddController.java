package com.devcamp.oddevennumberapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class oddController {
    @GetMapping("/checknumber")
    public String checkOdd(@RequestParam(required = true, name = "number") int number) {
        if (number % 2 == 0) {
            return "this is even number";
        } else {
            return "this is odd number";
        }
    }

}
